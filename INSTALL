
# Install LiberaForms

## Install mongodb
apt-get install mongodb

## Clone LiberaForms and create config.cfg

git clone https://gitlab.com/la-loka/liberaforms.git /opt/LiberaForms
cd /opt/LiberaForms
cp config.example.cfg config.cfg

And edit /opt/LiberaForms/config.cfg

You can create a SECRET_KEY like this

openssl rand -base64 32


## Create a python3 venv

If you have Python3 on your host

apt-get install python3-venv
python3 -m venv /opt/LiberaForms/venv

Or if you have old python2 on your host

apt-get install virtualenv
virtualenv /opt/LiberaForms/venv --python=python3



## Install python packages

source /opt/LiberaForms/venv/bin/activate
pip install --upgrade setuptools
pip install wheel
pip install -r /opt/LiberaForms/requirements.txt
pip install gunicorn

### File permissions
Admins can upload a logo. You need to give the system user who runs LiberaForms write permission

chown -R www-data /opt/LiberaForms/liberaforms/static/images

LiberaForms saves session data in files. www-data needs to write there too. 

chown -R www-data /opt/LiberaForms/flask_session


### Test your installation
source /opt/LiberaForms/venv/bin/activate
gunicorn -c /opt/LiberaForms/gunicorn.py liberaforms:app


## Install Supervisor to keep LiberaForms running
apt-get install supervisor

Edit /etc/supervisor/conf.d/LiberaForms.conf

[program:LiberaForms]
command = /opt/LiberaForms/venv/bin/gunicorn -c /opt/LiberaForms/gunicorn.py liberaforms:app
directory = /opt/LiberaForms
user = www-data


### Restart supervisor and check if LiberaForms is running

systemctl restart supervisor
supervisorctl status LiberaForms


## Debug LiberaForms

supervisorctl stop LiberaForms
cd /opt/LiberaForms
source /opt/LiberaForms/venv/bin/activate
python run


# Configure nginx proxy

## edit /etc/hosts and add an entry to point to your gunicorn process ip

127.0.0.1	liberaforms


## edit a new nginx hosts file

server {
    listen         80;
    server_name    my_domain.com;
    return         301 https://$server_name$request_uri;
}
server {
    listen 443 ssl;    
    server_name my_domain.com;

    ssl_certificate           /etc/letsencrypt/live/my_domain.com/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/my_domain.com/privkey.pem;
    ssl_dhparam               /etc/letsencrypt/ssl-dhparams.pem;

    location / {
        proxy_pass          http://liberaforms:5000;
        proxy_set_header    Host    $host;
        proxy_set_header    X-Forwarded-For $remote_addr;
        proxy_set_header    X-Real-IP   $remote_addr;
        proxy_pass_header   server;        
    }
    access_log /var/log/nginx/liberaforms.access.log;
    error_log /var/log/nginx/liberaforms.error.log notice;
}

## Database backup

Run this and check if a copy is dumped correctly.

```
/usr/bin/mongodump --db=LiberaForms --out="/var/backups/"
```

Add a line to your crontab to run it every night.

```
30 3 * * * /usr/bin/mongodump --db=LiberaForms --out="/var/backups/"
```
Note: This overwites the last copy. You might want to change that.
